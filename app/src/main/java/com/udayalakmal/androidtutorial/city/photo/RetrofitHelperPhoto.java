package com.udayalakmal.androidtutorial.city.photo;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Lakmal on 3/14/2018.
 */

public class RetrofitHelperPhoto {

    public PhotoService getPhotoService(){
        final Retrofit retrofit = createRetrofit();
        return retrofit.create(PhotoService.class);
    }



    /**
     * This custom client will append the "username=demo" query after every request.
     */
    private OkHttpClient createOkHttpClient() {
        final OkHttpClient.Builder httpClient =
                new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                final Request original = chain.request();
                final HttpUrl originalHttpUrl = original.url();

                final HttpUrl url = originalHttpUrl.newBuilder()
                        .addQueryParameter("username", "demo")
                        .build();

                // Request customization: add request headers
                final Request.Builder requestBuilder = original.newBuilder()
                        .url(url);

                final Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        return httpClient.build();
    }

    /**
     * Creates a pre configured Retrofit instance
     */

    private Retrofit createRetrofit() {

        return new Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/photos/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create()) // <- add this
                .client(createOkHttpClient())
                .build();

    }

    public static List<Photo> getPhotoList(){
        List<Photo> photoList = new ArrayList<>();
        com.squareup.okhttp.OkHttpClient client = new com.squareup.okhttp.OkHttpClient();


        com.squareup.okhttp.Request request = new com.squareup.okhttp.Request.Builder()
                .url("https://jsonplaceholder.typicode.com/photos/1/")
                .build();

        try {
            com.squareup.okhttp.Response response = client.newCall(request).execute();
            JSONObject jsonObject = new JSONObject(response.body().string());
         //   JSONArray jsonArray= new JSONArray(response);

//
//            for(int i=0 ; i< jsonArray.length();i++){
//
//                Photo photo = new Photo();
//                photo.albumId =
//                dogs.add(jsonArray.getString(i));
//            }
            // return dogs;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return photoList;
    }
}
