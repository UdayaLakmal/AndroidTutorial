package com.udayalakmal.androidtutorial.city;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.udayalakmal.androidtutorial.R;
import com.udayalakmal.androidtutorial.city.network.CityResponse;
import com.udayalakmal.androidtutorial.city.network.CityService;
import com.udayalakmal.androidtutorial.city.network.Geoname;
import com.udayalakmal.androidtutorial.city.network.RetrofitHelper;
import com.udayalakmal.androidtutorial.city.photo.Photo;
import com.udayalakmal.androidtutorial.city.photo.PhotoResponse;
import com.udayalakmal.androidtutorial.city.photo.PhotoService;
import com.udayalakmal.androidtutorial.city.photo.RetrofitHelperPhoto;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;


public class CityActivity extends AppCompatActivity {

    /**
     * We will query geonames with this service
     */
    @NonNull
    private CityService mCityService;

    private PhotoService mPhotoService;

    /**
     * Collects all subscriptions to unsubscribe later
     */
    @NonNull
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    private TextView mOutputTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_activities);

        mOutputTextView = (TextView) findViewById(R.id.output);

        // Initialize the city endpoint
        mCityService = new RetrofitHelper().getCityService();

        mPhotoService = new RetrofitHelperPhoto().getPhotoService();
        // Trigger our request and display afterwards
       //requestGeonames();

        requestPhotoes();
    }

    @Override
    protected void onDestroy() {
        // DO NOT CALL .dispose()
        mCompositeDisposable.clear();
        super.onDestroy();
    }


    private void displayGPhotos(@NonNull final List<Photo> photes) {
        // Cheap way to display a list of Strings - I was too lazy to implement a RecyclerView
        final StringBuilder output = new StringBuilder();
        if(photes!=null){
            for (final Photo photo : photes) {
                output.append(photo.title).append("\n");
            }

            mOutputTextView.setText(output.toString());
        }else{
            mOutputTextView.setText("Photoes not available");
        }

    }

    private void displayGeonames(@NonNull final List<Geoname> geonames) {
        // Cheap way to display a list of Strings - I was too lazy to implement a RecyclerView
        final StringBuilder output = new StringBuilder();
        if(geonames!=null){
            for (final Geoname geoname : geonames) {
                output.append(geoname.name).append("\n");
            }

            mOutputTextView.setText(output.toString());
        }else{
            mOutputTextView.setText("City Names not available");
        }

    }

    private void requestPhotoes(){
        mCompositeDisposable.add(mPhotoService.queryPhotos()
                .subscribeOn(Schedulers.io()) // "work" on io thread
                .observeOn(AndroidSchedulers.mainThread()) // "listen" on UIThread
                .map(new Function<PhotoResponse, List<Photo>>() {
                    @Override
                    public List<Photo> apply(
                            @io.reactivex.annotations.NonNull final PhotoResponse photoResponse)
                            throws Exception {
                        // we want to have the geonames and not the wrapper object
                        if(photoResponse!= null){
                            try {
                                Log.d("Test", photoResponse.toString()) ;

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        return photoResponse.photes;
                }
                })
                .subscribe(new Consumer<List<Photo>>() {
                    @Override
                    public void accept(
                            @io.reactivex.annotations.NonNull final List<Photo> photos)
                            throws Exception {
                        displayGPhotos(photos);
                    }
                })
        );
    }

    private void requestGeonames() {
        mCompositeDisposable.add(mCityService.queryGeonames(44.1, -9.9, -22.4, 55.2, "de")
                .subscribeOn(Schedulers.io()) // "work" on io thread
                .observeOn(AndroidSchedulers.mainThread()) // "listen" on UIThread
                .map(new Function<CityResponse, List<Geoname>>() {
                    @Override
                    public List<Geoname> apply(
                            @io.reactivex.annotations.NonNull final CityResponse cityResponse)
                            throws Exception {
                        // we want to have the geonames and not the wrapper object
                        return cityResponse.geonames;
                    }
                })
                .subscribe(new Consumer<List<Geoname>>() {
                    @Override
                    public void accept(
                            @io.reactivex.annotations.NonNull final List<Geoname> geonames)
                            throws Exception {
                        displayGeonames(geonames);
                    }
                })
        );
    }
}