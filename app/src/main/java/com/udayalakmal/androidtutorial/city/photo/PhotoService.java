package com.udayalakmal.androidtutorial.city.photo;

import io.reactivex.Single;
import retrofit2.http.GET;

/**
 * Created by Lakmal on 3/14/2018.
 */

public interface PhotoService {
    @GET ("1")
    Single<PhotoResponse> queryPhotos();

}
