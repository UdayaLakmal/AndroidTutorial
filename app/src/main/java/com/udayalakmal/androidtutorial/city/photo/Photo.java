package com.udayalakmal.androidtutorial.city.photo;

/**
 * Created by Lakmal on 3/14/2018.
 */

import com.google.gson.annotations.SerializedName;

/**
 *  {
 "albumId": 1,
 "id": 1,
 "title": "accusamus beatae ad facilis cum similique qui sunt",
 "url": "http://placehold.it/600/92c952",
 "thumbnailUrl": "http://placehold.it/150/92c952"
 }
 */
public class Photo {
    @SerializedName("albumId") public int albumId;
    @SerializedName("id") public int id;
    @SerializedName("title") public String title;
    @SerializedName("url") public String url;
    @SerializedName("thumbnailUrl") public String thumbnailUrl;
}
