package com.udayalakmal.androidtutorial.dogs;

/**
 * Created by Lakmal on 3/11/2018.
 */

import android.content.Context;
import android.os.SystemClock;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Retrofit;

public class RestClientDogs {
    private Context mContext;

    public RestClientDogs(Context context) {
        mContext = context;
    }

    public List<String> getFavoriteBooks() {
        //SystemClock.sleep(8000);// "Simulate" the delay of network.
        //get actual dogs
        return createBooks();
    }

    public List<String> getFavoriteBooksWithException() {
        SystemClock.sleep(8000);// "Simulate" the delay of network.
        throw new RuntimeException("Failed to load");
    }

    private List<String> createBooks() {
        List<String> books = new ArrayList<>();
        books.add("Lord of the Rings");
        books.add("The dark elf");
        books.add("Eclipse Introduction");
        books.add("History book");
        books.add("Der kleine Prinz");
        books.add("7 habits of highly effective people");
        books.add("Other book 1");
        books.add("Other book 2");
        books.add("Other book 3");
        books.add("Other book 4");
        books.add("Other book 5");
        books.add("Other book 6");
        return books;
    }

    public final static String BASE_URL_DOGS = "https://dog.ceo/api/breed/hound/images";

    public static dogsInterface create(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL_DOGS)
                .build();

        dogsInterface service = retrofit.create(dogsInterface.class);
        //service.fetchDogs(BASE_URL_DOGS);
       // Call<String> repos = service.fetchDogs(BASE_URL_DOGS);
       // dogsInterface service = retrofit.create(dogsInterface.class);
        return service;
    }
    private List<String> getDogs() {


        OkHttpClient client = new OkHttpClient();


            Request request = new Request.Builder()
                    .url(BASE_URL_DOGS)
                    .build();


        List<String> dogs = new ArrayList<>();
        try {
            Response response = client.newCall(request).execute();
            JSONObject jsonObject = new JSONObject(response.body().string());
            JSONArray jsonArray= jsonObject.getJSONArray("message");

            for(int i=0 ; i< jsonArray.length();i++){
                dogs.add(jsonArray.getString(i));
            }
            // return dogs;
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return dogs;
//        RequestQueue queue = Volley.newRequestQueue(contcext);
//        String url =BASE_URL_DOGS;
//        List<String> dogs = new ArrayList<>();
//// Request a string response from the provided URL.
//        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        // Display the first 500 characters of the response string.
//                        //mTextView.setText("Response is: "+ response.substring(0,500));
//                        try {
//                            JSONObject jsonObject = new JSONObject(response);
//                            JSONArray jsonArray= jsonObject.getJSONArray("message");
//
//                            for(int i=0 ; i< jsonArray.length();i++){
//                                dogs.add(jsonArray.getString(i));
//                            }
//                           // return dogs;
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//               // mTextView.setText("That didn't work!");
//            }
//        });

// Add the request to the RequestQueue.
//        queue.add(stringRequest);

//        List<String> books = new ArrayList<>();
//        books.add("Lord of the Rings");
//        books.add("The dark elf");
//        books.add("Eclipse Introduction");
//        books.add("History book");
//        books.add("Der kleine Prinz");
//        books.add("7 habits of highly effective people");
//        books.add("Other book 1");
//        books.add("Other book 2");
//        books.add("Other book 3");
//        books.add("Other book 4");
//        books.add("Other book 5");
//        books.add("Other book 6");
//        return books;
    }
    public List<String> getFavoriteDogs() {

        return getDogs();
    }
}