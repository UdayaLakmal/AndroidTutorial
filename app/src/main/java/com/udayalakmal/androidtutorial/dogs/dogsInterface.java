package com.udayalakmal.androidtutorial.dogs;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by Lakmal on 3/13/2018.
 */

public interface dogsInterface {
    @GET
    Call<String> fetchDogs(@Url String url);


}
