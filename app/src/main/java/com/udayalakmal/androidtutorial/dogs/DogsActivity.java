package com.udayalakmal.androidtutorial.dogs;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.udayalakmal.androidtutorial.R;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DogsActivity extends AppCompatActivity {

    private Disposable dogsSubscription;
    private RecyclerView dogssRecyclerView;
    private ProgressBar progressBar;
    private DogsAdapter stringAdapterDogs;
    private RestClientDogs restClientDogs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        restClientDogs = new RestClientDogs(this);
        configureLayout();
        createObservable();
    }
    @NonNull
    private dogsInterface dogsInterfacee;
    @NonNull
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    private void createObservable() {

        Observable<List<String>> dogssObservable =
                Observable.fromCallable(() -> restClientDogs.getFavoriteDogs());
        dogsSubscription = dogssObservable.
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(strings -> displayDogs(strings));

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dogsSubscription != null && !dogsSubscription.isDisposed()) {
            dogsSubscription.dispose();
        }
    }

    private void displayDogs(List<String> books) {
        stringAdapterDogs.setStrings(books);
        progressBar.setVisibility(View.GONE);
        dogssRecyclerView.setVisibility(View.VISIBLE);
    }

    private void configureLayout() {
        setContentView(R.layout.activity_dogs);
        progressBar = (ProgressBar) findViewById(R.id.loader);
        dogssRecyclerView = (RecyclerView) findViewById(R.id.dogs_list);
        dogssRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        stringAdapterDogs = new DogsAdapter(this);
        dogssRecyclerView.setAdapter(stringAdapterDogs);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (dogsSubscription != null && !dogsSubscription.isDisposed()) {
            dogsSubscription.dispose();
        }
    }
}
